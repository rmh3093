package ryanhope.us.motsim;
/**
 * MOTSIM_View.java
 * motsim
 */

/*
 Copyright (c) 2008, Ryan M. Hope
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name of the project nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.awt.Dimension;
//import java.awt.DisplayMode;
import java.awt.AWTException;
import java.awt.Cursor;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;

public class View {

	Model model;
	ControlPanel cp;
	Trial trial;
	Dimension screen_size;
	EventHandler evh;
	Robot robot;
	Image cursorImage;
	Cursor blank_cursor;

	public View(Model model) {
		this.model = model;
		screen_size = Toolkit.getDefaultToolkit().getScreenSize();
		cursorImage = Toolkit.getDefaultToolkit().getImage("xparent.gif");
		blank_cursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage,new Point(0,0),"");
		model.ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		model.gd = model.ge.getDefaultScreenDevice();
		try {
			robot = new Robot(model.gd);
		} catch (AWTException e) {
		}
	}
	
	public void resetMouse() {
		robot.mouseMove(screen_size.width/2, screen_size.height/2);
	}
	
	public void setEventHandler(EventHandler evh) {
		this.evh = evh;
	}

	public void startControlPanel() {	
		cp = new ControlPanel(evh);
	}

	public void startTrial() {
		trial = new Trial(model, this, screen_size, evh);
	}

	public void collectDemographics() {
		cp.config_panel.setVisible(false);
		cp.sx_panel.setVisible(true);
		cp.pack();
	}

}
