package ryanhope.us.motsim;
/**
 * Results.java
 * motsim
 */

/*
Copyright (c) 2008, Ryan M. Hope
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
	  this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
 * Neither the name of the project nor the names of its contributors may be
	  used to endorse or promote products derived from this software without
	  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.io.PrintWriter;
import java.util.Calendar;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.SAXException;

import com.sun.xml.internal.bind.util.AttributesImpl;

public class Results {

	public void writeResultsXML(Participant p) {

		try {
			/*File file = new File(p.fullname.replaceAll(" ", "_") + 
					Calendar.YEAR + "-" + Calendar.MONTH + "-" +
					Calendar.DAY_OF_MONTH + "_" + Calendar.HOUR + 
					Calendar.MINUTE + "_" + ".xml");*/
			PrintWriter out = null;
			out = new PrintWriter(System.out);
			StreamResult streamResult = new StreamResult(out);
			SAXTransformerFactory tf = (SAXTransformerFactory) SAXTransformerFactory.newInstance();
			tf.setAttribute("indent-number", new Integer(2));
			TransformerHandler hd = null;
			hd = tf.newTransformerHandler();
			Transformer serializer = hd.getTransformer();
			serializer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
			serializer.setOutputProperty(OutputKeys.INDENT,"yes");
			hd.setResult(streamResult);
			hd.startDocument();
			AttributesImpl atts = new AttributesImpl();

			hd.startElement("","","MOTSIM",atts);

			atts.clear();
			atts.addAttribute("","","NAME","CDATA",p.fullname);
			atts.addAttribute("","","EMAIL","CDATA",p.email);
			atts.addAttribute("","","GENDER","CDATA",p.gender);
			atts.addAttribute("","","AGE","CDATA",String.valueOf(p.age));
			atts.addAttribute("","","PRIMARY_HAND","CDATA",p.primary_hand);
			atts.addAttribute("","","VISION","CDATA",p.good_vision);
			atts.addAttribute("","","TIMESTAMP","CDATA",Calendar.getInstance().getTime().toString());
			hd.startElement("","","PARTICIPANT",atts);
			
			int j = 0;
			for (int i : p.trialResults) {
				j++;
				atts.clear();
				atts.addAttribute("","","TRIAL","CDATA",String.valueOf(j));
				atts.addAttribute("","","REACTION_TIME_MS","CDATA",String.valueOf(i));
				hd.startElement("","","RESULT",atts);
				hd.endElement("","","RESULT");
			}
			
			hd.endElement("","","PARTICIPANT");
			
			hd.endElement("","","MOTSIM");
			hd.endDocument();

		} catch (SAXException e) {
		//} catch (FileNotFoundException e) {
		} catch (TransformerConfigurationException e) {
		}

	}



}
