package ryanhope.us.motsim;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

@SuppressWarnings("serial")
class ControlPanel extends JFrame implements ActionListener {
	
	EventHandler evh;

	JPanel config_panel, config_subpanel, sx_panel, sx_subpanel, options_panel, 
	config_start_panel, sx_sex_panel, sx_handedness_panel, sx_vision_panel, 
	sx_start_panel;
	JLabel targets_label, duration_label, speed_label,
	simmode_label, trials_label, entropy_label, fontsize_label, 
	sx_name, sx_sex, sx_age, sx_handedness, sx_vision, sx_email;
	JSpinner targets_spinner, duration_spinner, speed_spinner, 
	trials_spinner, fontsize_spinner;
	JComboBox simmode_combobox, entropy_combobox, gender_combobox, 
	vision_combobox, handedness_combobox;
	JCheckBox collectdata_checkbox, confirm_cb;
	JButton config_start_button, sx_start_button;
	JTextField sx_name_textfield, sx_age_textfield, sx_email_textfield;

	String[] entropyStyles = {"None", "Low", "Medium", "High"};
	String[] simModes = {"Single Trial","Multiple Trials","Continuous"};
	String[] genders = {"", "Male", "Female"};
	String[] vision = {"", "Yes", "No"};
	String[] handedness = {"", "Right", "Left"};
	String[] tlx = {"Mental Demand", "Physical Demand", "Temporal Demand", 
			"Performance", "Effort", "Fustration" };

	public ControlPanel(EventHandler evh) {
		
		this.evh = evh;
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("MOTSIM");
		setResizable(false);
		setLocationRelativeTo(null);
		setLayout(new FlowLayout(FlowLayout.CENTER,10,10));
		
		sx_panel = new JPanel();
		sx_panel.setVisible(false);
		sx_subpanel = new JPanel(new GridLayout(0,2));
		sx_subpanel.setBorder(BorderFactory.createTitledBorder(
				new CompoundBorder(BorderFactory.createTitledBorder(
						"Participant Information"), new EmptyBorder(10,20,10,20)))); 
		sx_name = new JLabel("Full Name: ");
		sx_name_textfield = new JTextField();
		sx_subpanel.add(sx_name);
		sx_subpanel.add(sx_name_textfield);
		sx_email = new JLabel("E-Mail: ");
		sx_email_textfield = new JTextField();
		sx_subpanel.add(sx_email);
		sx_subpanel.add(sx_email_textfield);
		sx_age = new JLabel("Age: ");
		sx_age_textfield = new JTextField();
		sx_subpanel.add(sx_age);
		sx_subpanel.add(sx_age_textfield);
		sx_sex = new JLabel("Sex: ");
		gender_combobox = new JComboBox(genders);
		sx_subpanel.add(sx_sex);
		sx_subpanel.add(gender_combobox);
		sx_handedness = new JLabel("Primary hand: ");
		sx_handedness_panel = new JPanel(new GridLayout(0,2));
		handedness_combobox = new JComboBox(handedness);
		sx_subpanel.add(sx_handedness);
		sx_subpanel.add(handedness_combobox);
		sx_vision = new JLabel("20/20 or better vision? ");
		sx_vision_panel = new JPanel(new GridLayout(0,2));
		vision_combobox = new JComboBox(vision);
		sx_subpanel.add(sx_vision);
		sx_subpanel.add(vision_combobox);
		sx_panel.add(sx_subpanel);
		sx_start_panel = new JPanel(new GridLayout(0,1));
		confirm_cb = new JCheckBox("Lock-in Information: ");
		confirm_cb.addActionListener(this);
		confirm_cb.setActionCommand("check-info");
		sx_start_panel.add(confirm_cb);
		sx_start_button = new JButton("Start");
		sx_start_button.setActionCommand("sx-start");
		sx_start_button.setEnabled(false);
		sx_start_button.addActionListener(evh);
		sx_start_panel.add(sx_start_button);
		sx_panel.add(sx_start_panel);

		config_panel = new JPanel();
		config_panel.setVisible(false);
		// Options Panel
		options_panel = new JPanel(new GridLayout(0,2));
		options_panel.setBorder(new CompoundBorder(
				BorderFactory.createTitledBorder("Configuration"),
				new EmptyBorder(10,20,10,20)));
		simmode_label = new JLabel("MOTSIM Mode: ");
		simmode_combobox = new JComboBox(simModes);
		simmode_combobox.setActionCommand("simmode");
		simmode_combobox.addActionListener(evh);
		simmode_combobox.setFocusable(false);
		options_panel.add(simmode_label);
		options_panel.add(simmode_combobox);
		trials_label = new JLabel("Number of trials: ");
		trials_spinner = 
			new JSpinner(new SpinnerNumberModel(5, 1, 10000, 1));
		trials_spinner.setEnabled(false);
		trials_spinner.setFocusable(false);
		options_panel.add(trials_label);
		options_panel.add(trials_spinner);
		targets_label = new JLabel("Number of targets: ");
		targets_spinner = 
			new JSpinner(new SpinnerNumberModel(2, 1, 50, 1));
		targets_spinner.setFocusable(false);
		options_panel.add(targets_label);
		options_panel.add(targets_spinner);
		fontsize_label = new JLabel("Font size: ");
		fontsize_spinner = 
			new JSpinner(new SpinnerNumberModel(14, 1, 38, 1));
		fontsize_spinner.setFocusable(false);
		options_panel.add(fontsize_label);
		options_panel.add(fontsize_spinner);
		speed_label = new JLabel("Velocity multiplier: ");
		speed_spinner = 
			new JSpinner(new SpinnerNumberModel(.5, -10, 10, .01));
		speed_spinner.setFocusable(false);
		options_panel.add(speed_label);
		options_panel.add(speed_spinner);
		duration_label = new JLabel("Trial duration (seconds): ");
		duration_spinner = 
			new JSpinner(new SpinnerNumberModel(5, 1, 300, 1));
		duration_spinner.setFocusable(false);
		options_panel.add(duration_label);
		options_panel.add(duration_spinner);
		entropy_label = new JLabel("Entropy: ");
		entropy_combobox = new JComboBox(entropyStyles);
		entropy_combobox.setFocusable(false);
		options_panel.add(entropy_label);
		options_panel.add(entropy_combobox);
		config_panel.add(options_panel);
		// Start Panel
		config_start_panel = new JPanel(new GridLayout(0,1));
		config_start_button = new JButton("Start");
		config_start_button.setActionCommand("cp-start");
		config_start_button.addActionListener(evh);
		config_start_button.setSelected(true);
		collectdata_checkbox = new JCheckBox("Collect Data", false);
		config_start_panel.add(collectdata_checkbox);
		config_start_panel.add(config_start_button);
		config_panel.add(config_start_panel);

		add(config_panel);
		add(sx_panel);
		
		pack();
	}

	private void checkDemographics() {
		if ((sx_name_textfield.getText().length() > 0) && 
				(sx_age_textfield.getText().length() > 0) &&
				(Integer.valueOf(sx_age_textfield.getText()) >= 18) &&
				(gender_combobox.getSelectedIndex() > 0) &&
				(handedness_combobox.getSelectedIndex() > 0) &&
				(vision_combobox.getSelectedIndex() > 0) &&
				(sx_email_textfield.getText().length() > 0)) { 
			sx_start_button.setEnabled(true);
			confirm_cb.setSelected(true);
			sx_name_textfield.setEnabled(false);
			sx_age_textfield.setEnabled(false);
			gender_combobox.setEnabled(false);
			handedness_combobox.setEnabled(false);
			vision_combobox.setEnabled(false);
			sx_email_textfield.setEnabled(false);
		} else {
			sx_start_button.setEnabled(false);
			confirm_cb.setSelected(false);
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand()=="check-info") {
			if (!confirm_cb.isSelected()) {
				confirm_cb.setSelected(false);
				sx_start_button.setEnabled(false);
				sx_name_textfield.setEnabled(true);
				sx_age_textfield.setEnabled(true);
				gender_combobox.setEnabled(true);
				handedness_combobox.setEnabled(true);
				vision_combobox.setEnabled(true);
				sx_email_textfield.setEnabled(true);
			} else {
				checkDemographics();
			}
		}			
	}
	
	void addStartListener(ActionListener l) {
		config_start_button.addActionListener(l);
		sx_start_button.addActionListener(l);
	}

}
