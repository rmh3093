package ryanhope.us.motsim;
/**
 * MOTSIM_Model.java
 * motsim
 */

/*
 Copyright (c) 2008, Ryan M. Hope
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name of the project nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.awt.Color;
import java.awt.DisplayMode;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;

public class Model {
	
	/* Graphic Environment Information */
	GraphicsEnvironment ge;
	GraphicsDevice gd;
	DisplayMode display_mode_default;
	DisplayMode display_mode_preferred;
	DisplayMode[] display_mode_available;	
	
	ArrayList<Target> targets;

	/*
	 * Use this enum to indicate which stage of the experiment we are in
	 */
	public enum TrialStage {
		WAIT,
		DEMOGRAPHICS,
		TLX_BIAS,
		GENERATE_TARGETS,
		START_TRIAL,
		TRIAL_SPLASH,
		MOVE_TARGETS,
		CHOOSE_TARGET,
		START_SEARCH,
		FIND_TARGET,
		STOP_SEARCH,
		TRIAL_CLEANUP,
		TLX,
		END
	}
	TrialStage stage = TrialStage.WAIT;

	/* Represents basic experiment information */
	Participant participant = null;
	int trialMax = 1;
	int trialCount = 1;
	int startTime = 0;
	int stopTime = 0;	
	
	/* Trial worker thread */
	Thread trialThread = null;
	
	/* GUI Information */
	String borderstyle;
	Font target_font;
	Font query_font;
	Font trial_splash_font;
	int query_font_size = 18;
	String defaultvelocities;
	String entropy;
	int duration = 0;
	int maxduration;
	int target = -1;
	int selected = -1;
	int hover = -1;
	boolean blink = false;
	Color target_color = Color.red;
	Color target_blink_color = Color.yellow;
	Color target_hover_color = Color.blue;
	Color target_selected_color = Color.green;
	Color query_color = new Color(255,255,102);
	int mouse_x = -1;
	int mouse_y = -1;
	int mouse_x_clk = -1;
	int mouse_y_clk = -1;
	boolean waiting = false;
	
}
