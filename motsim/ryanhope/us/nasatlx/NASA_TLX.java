/**
 * NASA_TLX.java
 */

/**
 * NASA-TLX is a subjective workload assessment tool. 
 * 
 * NASA-TLX allows users to perform subjective workload assessments on 
 * operator(s) working with various human-machine systems. NASA-TLX is a 
 * multi-dimensional rating procedure that derives an overall workload score 
 * based on a weighted average of ratings on six subscales.
 * 
 * These subscales include Mental Demands, Physical Demands, Temporal Demands, 
 * Own Performance, Effort and Frustration. It can be used to assess workload 
 * in various human-machine environments such as aircraft cockpits, command, 
 * control, and communication (C3) workstations; supervisory and process 
 * control environments; simulations and laboratory tests.
 * 
 * http://humansystems.arc.nasa.gov/groups/TLX/downloads/NASA-TLXChapter.pdf
 * 
 * Hart, S.G. & Staveland, L.E. (1988). Development of NASA-TLX 
 * 		(Task Load Index): Results of empirical and theoretical research. In 
 * 		 P.A. Hancock & N. Meshkati (Eds.), Human Mental Workload (pp. 239-250). 
 * 		 Amsterdam: North Holland Press.
 * 
 * 
 * @author Ryan M. Hope <rmh3093@gmail.com>
 *
 */

package ryanhope.us.nasatlx;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSlider;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ryanhope.us.util.CombinationGenerator;

import jargs.gnu.CmdLineParser;

public class NASA_TLX {

	static String[] default_subscales = {
		"Mental Demand",
		"Physical Demand",
		"Temporal Demand",
		"Own Performance",
		"Effort",
		"Fustration"
	};

	SystemTray systray;
	TrayIcon trayIcon;
	JPopupMenu popupmenu;
	int tasks = 0;
	boolean verbose = false;
	String[] subscales;
	Integer[][] subscale_pairs;
	TreeMap<String, Integer> biasTally;
	Vector<TreeMap<String, Integer>> taskRatings;

	public int scoreTask(TreeMap<String, Integer> taskRating) {
		int score = 0;
		int sum = 0;
		for (String s : subscales) {
			int r = taskRating.get(s); 
			int w = biasTally.get(s);
			sum += w;
			score += (r * w);
		} 
		return score / sum;
	}
	
	public void printBiasTally() {
		for (String s : subscales)
			System.out.println(s + " = " + biasTally.get(s));
	}
	
	public void printTaskRatings() {
		System.out.println();
		for (int i=0; i<taskRatings.size(); i++) {
			TreeMap<String, Integer> taskRating = taskRatings.get(i);
			System.out.println("\n~Task " + (i+1) + "~\n");
			for (String s : subscales)
				System.out.println(s + " = " + taskRating.get(s));
			System.out.println("_____________________");
			System.out.println("Workload Score = " + scoreTask(taskRating));
		}
	}

	private Integer[][] generateComparisons(String[] elements) {
		int[] indices;
		Vector<Integer[]> pairs = new Vector<Integer[]>();
		CombinationGenerator cg = 
			new CombinationGenerator(elements.length, 2);
		while (cg.hasMore()) {
			indices = cg.getNext();
			Integer[] pair = {indices[0],indices[1]};
			pairs.add(pair);
		}
		Integer[][] subscale_pairs = new Integer[pairs.size()][];
		for (int i=0; i<pairs.size(); i++) {
			Integer[] p = pairs.get(i);
			subscale_pairs[i] = p;
		}
		return subscale_pairs;	
	}

	@SuppressWarnings("serial")
	public class TaskRating extends JFrame implements ActionListener {
		
		Hashtable<Integer, JLabel> labelTable;
		TRPanel[] panels;
		JPanel left, right;
		JButton button;
		
		class TRPanel extends JPanel {
			JSlider tlx_slider;
			String title;
			public TRPanel(String subscale) {
				title = subscale;
				setBorder(
						BorderFactory.createTitledBorder(
								new CompoundBorder(
										BorderFactory.createTitledBorder(
												BorderFactory.createEtchedBorder(),
												title, TitledBorder.CENTER, 
												TitledBorder.CENTER), 
												new EmptyBorder(10,20,10,20))));
				setLayout(new GridLayout(2,1));
				tlx_slider = new JSlider(0,100,50);
				tlx_slider.setMajorTickSpacing(10);
				tlx_slider.setLabelTable(labelTable);
				tlx_slider.setPaintTicks(true);
				tlx_slider.setPaintLabels(true);
				add(tlx_slider);
			}
		}

		public TaskRating() {
			taskRatings = new Vector<TreeMap<String, Integer>>();
			setTitle("Task Rating");
			setLocationRelativeTo(null);
			setResizable(false);
			setLayout(new FlowLayout());
			left = new JPanel(new GridLayout(0,2,10,10));
			right = new JPanel(new FlowLayout());
			labelTable = new Hashtable<Integer, JLabel>();
			labelTable.put( new Integer(0), new JLabel("Min") );
			labelTable.put( new Integer(100), new JLabel("Max") );
			panels = new TRPanel[subscales.length];
			for (int i=0; i<subscales.length; i++) {
				panels[i] = new TRPanel(subscales[i]);
				left.add(panels[i]);
			}
			button = new JButton("Done");
			button.addActionListener(this);
			right.add(button);
			add(left);
			add(right);
			pack();
			setVisible(true);
		}
		
		public void actionPerformed(ActionEvent e) {
			TreeMap<String, Integer> taskRating = 
				new TreeMap<String, Integer>();
			for (int i=0; i<panels.length; i++) {
				taskRating.put(panels[i].title, panels[i].tlx_slider.getValue());
			}
			taskRatings.add(taskRating);
			if (verbose)
				printTaskRatings();
			dispose();
		};

	}

	@SuppressWarnings("serial")
	public class PairWiseComparison
	extends JFrame implements ActionListener {

		PWCPanel[] panels;
		int pair = 0;

		class PWCPanel extends JPanel {

			JButton b1,b2;

			public PWCPanel(String subscale1, String subscale2, ActionListener al) {
				setDefaultCloseOperation(EXIT_ON_CLOSE);
				setLayout(new GridLayout(2,1,15,15));
				b1 = new JButton(subscale1);
				b1.setActionCommand(subscale1);
				b1.addActionListener(al);
				b1.setPreferredSize(new Dimension(150,40));
				b1.setFocusable(false);
				b2 = new JButton(subscale2);
				b2.setActionCommand(subscale2);
				b2.addActionListener(al);
				b2.setPreferredSize(new Dimension(200,40));
				b2.setFocusable(false);
				add(b1);
				add(b2);
				setVisible(false);
			}

		}

		/**
		 * Participant selects the member of each pair that provides the most
		 * significant source of workload variation in the tasks
		 * 
		 * @param verbose If true the results will be printed to stdout.
		 */
		public PairWiseComparison () {
			subscale_pairs = generateComparisons(subscales);
			biasTally = new TreeMap<String, Integer>();
			for (String s : subscales) biasTally.put(s, 0);
			setTitle("Pair-wise Comparison");
			setLocationRelativeTo(null);
			setResizable(false);
			setPreferredSize(new Dimension(250,90));
			setLayout(new FlowLayout(FlowLayout.CENTER,10,10));
			panels = new PWCPanel[subscale_pairs.length];
			for (int i=0; i<subscale_pairs.length; i++) {
				panels[i] = new PWCPanel(subscales[subscale_pairs[i][0]], 
						subscales[subscale_pairs[i][1]],this);
				add(panels[i]);
			}
			panels[pair].setVisible(true);
			pack();
			setVisible(true);
		}


		public void actionPerformed(ActionEvent e) {
			int oldval = biasTally.get(e.getActionCommand());
			biasTally.put(e.getActionCommand(), oldval+1);
			panels[pair].setVisible(false);
			pair++;
			if (pair<subscale_pairs.length)
				panels[pair].setVisible(true);
			else {
				if (verbose) printBiasTally();
				new TaskRating();
				dispose();
			}
		}

	}

	private void printUsage() {
		System.err.println("Usage: NASA_TLX [OPTIONS]\n");
		System.err.println("Option\t\tGNU long option\t\tMeaning");
		System.err.println("-v\t\t--verbose\t\tEnable verbose output");
		System.err.println("-t\t\t--tasks number\t\tThe number of subtasks");
		System.err.println("-d\t\t--disable subscale\tDisable a subscale");
		System.err.println("-s\t\t--systray\t\tRun in systray");
		System.err.println("\nAvailable subscales:");
		for (int i=0; i<default_subscales.length; i++)
			System.err.println("  " + (i+1) + ". " + default_subscales[i]);
	}
	
	private void setupSystray() {
		systray = SystemTray.getSystemTray();
		Image image = Toolkit.getDefaultToolkit().getImage("ryanhope/us/" +
				"nasatlx/nasa-large.png").getScaledInstance(24, 24, 
						Image.SCALE_SMOOTH);
		popupmenu = new JPopupMenu("NASA-TLX");
		JMenuItem newparticipant = new JMenuItem("New Participant");
		popupmenu.add(newparticipant);
		popupmenu.addSeparator();
		JMenuItem pairwise = new JMenuItem("Bias Rating");
		JMenuItem rating = new JMenuItem("Task Rating");
		popupmenu.add(pairwise);
		popupmenu.add(rating);
		popupmenu.addSeparator();
		popupmenu.add(new JMenuItem("Exit") {{
	    	addActionListener(new ActionListener() {
	    		public void actionPerformed(ActionEvent e) {
		        	System.exit(0);
	    		}
	    	}
    		);
	    }});
		try {
			trayIcon = new TrayIcon(image, "NASA-TLX",null);
			trayIcon.addMouseListener(new MouseAdapter() {
		    	public void mouseReleased(MouseEvent e) {
		    		popupmenu.setVisible(false);
		    		if(e.isPopupTrigger()) {
		    			popupmenu.setInvoker(popupmenu);
		    			popupmenu.setLocation(e.getX(), e.getY());
		    			popupmenu.setVisible(true);
		    		}
		    	}
		    	public void mousePressed(MouseEvent e) {
		    		mouseReleased(e);
		    	}
		    });
			systray.add(trayIcon);			
		} catch (AWTException e) {
		}
	}
	
	static public void daemonize() {
	   System.out.close();
	   System.err.close();
	}
	
	public NASA_TLX(String[] args) {

		CmdLineParser parser = new CmdLineParser();
		CmdLineParser.Option tasks = parser.addIntegerOption('t', "tasks");
		CmdLineParser.Option disable = parser.addIntegerOption('d', "disable");
		CmdLineParser.Option systray = parser.addBooleanOption('s', "systray");
		CmdLineParser.Option verbose = parser.addBooleanOption('v', "verbose");
		CmdLineParser.Option help = parser.addBooleanOption('h', "help");

		try {
			parser.parse(args);
		}
		catch ( CmdLineParser.OptionException e ) {
			System.err.println(e.getMessage());
			printUsage();
			System.exit(2);
		}

		if ((Boolean)parser.getOptionValue(help, false)) {
			printUsage();
			System.exit(2);
		}
		
		this.tasks = (Integer)parser.getOptionValue(tasks, 1);
		this.verbose = (Boolean)parser.getOptionValue(verbose, false);
		Vector<Integer> disabled_subscales = parser.getOptionValues(disable);
		if (disabled_subscales.isEmpty())
			subscales = default_subscales;
		else {
			subscales = 
				new String[default_subscales.length-disabled_subscales.size()];
			boolean found;
			int index = 0;
			for (int i=0; i<default_subscales.length; i++) {
				found = false;
				int idx = 0;
				while (!found && idx<disabled_subscales.size()) {
					if ((disabled_subscales.get(idx)-1)==i) found = true;
					else idx++;
				}
				if (!found) {
					subscales[index] = default_subscales[i];
					index++;
				}
			}
		}			
		subscale_pairs = generateComparisons(subscales);
		
		if (SystemTray.isSupported() && 
				(Boolean)parser.getOptionValue(systray, false)) {
			daemonize();
			setupSystray();
		} else {
			new PairWiseComparison();
		}
	}	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {		
		new NASA_TLX(args);
	}

}
