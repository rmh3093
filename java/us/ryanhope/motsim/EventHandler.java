package us.ryanhope.motsim;
/**
 * EventHandler.java
 * motsim
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import us.ryanhope.experiment.ParticipantGUI;

public class EventHandler
implements WindowListener, MouseMotionListener, MouseListener, KeyListener, 
ActionListener {

	View view;
	Model model;
	Controller controller;

	public EventHandler(Model model, View view, Controller controller) {
		this.model = model;
		this.view = view;
		this.controller = controller;
		view.setEventHandler(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand()=="cp-start") {
			if ((view.cp.collectdata_checkbox.isSelected()) &&
					!(view.cp.sx_panel.isVisible())) {
				view.collectDemographics();
				return;
			} else {
				view.cp.sx_panel.setVisible(false);
			}
		} else if (e.getActionCommand()=="sx-start") {
			/* Store participant information */
		/*	model.participant = new ParticipantGUI(
					view.cp.sx_name_textfield.getText(),
					view.cp.sx_email_textfield.getText(),
					view.cp.genders[view.cp.gender_combobox.getSelectedIndex()],
					view.cp.handedness[view.cp.handedness_combobox.getSelectedIndex()],
					view.cp.vision[view.cp.vision_combobox.getSelectedIndex()],
					Integer.valueOf(view.cp.sx_age_textfield.getText())
			);*/
		}
		if (e.getActionCommand()=="cp-start" || 
				e.getActionCommand()=="sx-start") {
			model.maxduration = 1000 * (Integer)view.cp.duration_spinner.getValue();
			model.entropy = view.cp.entropy_combobox.getSelectedItem().toString();
			model.stage = Model.TrialStage.START_TRIAL;
			synchronized (model.trialThread) {
				model.trialThread.notifyAll();
			}
		} else if (e.getActionCommand()=="simmode") {
			if (view.cp.simmode_combobox.getSelectedItem().toString() == 
			"Multiple Trials") {
				view.cp.trials_spinner.setEnabled(true);
			} else {
				view.cp.trials_spinner.setEnabled(false);
			}	
		}
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void mouseClicked(MouseEvent e) {
		if (model.stage == Model.TrialStage.FIND_TARGET) {
			model.mouse_x_clk = e.getX();
			model.mouse_y_clk = e.getY();
			synchronized (model.trialThread) {
				model.trialThread.notifyAll();
			}
		}
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
		if (model.stage == Model.TrialStage.FIND_TARGET) {
			model.mouse_x = e.getX();
			model.mouse_y = e.getY();
			synchronized (model.trialThread) {
				model.trialThread.notifyAll();
			}
		}
	}

	public void windowActivated(WindowEvent e) {			
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowClosing(WindowEvent e) {
	}

	public void windowDeactivated(WindowEvent e) {
		model.stage = Model.TrialStage.WAIT;
		view.trial.stopTrial();
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowOpened(WindowEvent e) {
	}

}
