package us.ryanhope.motsim;
/**
 * Controller.java
 * motsim
 */

/*
 Copyright (c) 2008, Ryan M. Hope
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name of the project nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.awt.Cursor;
import java.awt.Font;
import java.awt.font.TextLayout;
import java.util.ArrayList;
import java.util.Random;

import us.ryanhope.experiment.Results;

public class Controller implements Runnable {

	Model model;
	View view;

	TrialDataDynamic tdd;
	Results results;
	
	Random rand;

	/**
	 * Create a new controller object for MOTSIM. MOTSIM_Controller does most
	 * of the grunt works for MOTSIM.
	 * 
	 * @param model MOTSIM_Model
	 * @param view MOTSIM_View
	 */
	public Controller(Model model, View view) {
		rand = new Random(); // A random number generator
		this.model = model;
		this.view = view;
		results = new Results();
		/* Generate some data dynamically for testing purposes */
		tdd = new TrialDataDynamic(view.screen_size);
		model.trialThread = new Thread(this);
		model.trialThread.start();
	}

	/**
	 * Update target positions based on X and Y velocities
	 */
	protected synchronized void moveTargets() {
		int sleep = 20; // Sleep for 20ms to maintain ~50fps
		int[] entropy = null;
		for (int i=0; i<model.targets.size(); i++) {
			Target t = model.targets.get(i);
			boolean flip = false;
			/*
			 * If target hits a border flip is angle and velocity so that it
			 * reflects like a mirror 
			 */
			if (((t.x + t.velX + t.bounds.getWidth()) > view.screen_size.width) || 
					((t.x + t.velX) < 0)) {
				t.velX = t.velX * -1;
				flip = true;
			}
			if (((t.y + t.velY) > view.screen_size.height-view.trial.mh) || 
					((t.y + t.velY - t.bounds.getHeight()) < 0)) {
				t.velY = t.velY * -1;
				flip = true;
			}
			if (flip==true) {
				t.moveAngle = t.moveAngle + 180;
				t.move = true;
			}

			/*
			 * Add any entropy to the move angle if necessary
			 */
			if (model.entropy != "None") {
				if (t.move == false) {
					if (model.entropy == "Low") {
						entropy = tdd.generateEntropyLow();
					} else if (model.entropy == "Medium") {
						entropy = tdd.generateEntropyMedium();
					} else if (model.entropy == "High") {
						entropy = tdd.generateEntropyHigh();
					}
					t.moveAngle = (t.moveAngle + entropy[rand.nextInt(10000)]) % 360;
					t.move = true;
				} else {
					t.steps++;
					if (t.steps > 5) {
						t.move = false;
						t.steps = 0;
					}
				}
				t.velX = tdd.calcAngleMoveX(t.moveAngle);
				t.velY = tdd.calcAngleMoveY(t.moveAngle);
			}
			t.x = t.x + t.velX * t.velocityMOD;
			t.y = t.y + t.velY * t.velocityMOD;
		}
		view.trial.repaint();
		try {
			model.duration = model.duration + sleep;
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
		}
	}

	private void generateTargets() {
		model.targets = new ArrayList<Target>(tdd.generate_targets(
				(Integer)view.cp.targets_spinner.getValue()));
		for (int i=0; i<model.targets.size(); i++) {
			Target t = model.targets.get(i);
			/* Adjust base speed of targets */
			t.velocityMOD = (Double)view.cp.speed_spinner.getValue();
			/* Get bounds of targets */
			TextLayout tl = new TextLayout(t.callsign, model.target_font, view.trial.frc);
			t.bounds = tl.getBounds();
			/* Make sure all targets start on screen */
			int max;
			max = view.screen_size.width - (int)Math.round(t.bounds.getWidth());
			if (t.x > max)
				t.x = max - 1;
			max = view.screen_size.height - view.trial.mh;
			if (t.y > max)
				t.y = max - 1;
			if (t.y < t.bounds.getHeight())
				t.y = t.bounds.getHeight() + 1;
		}
	}
	
	/**
	 * Main Trial Process
	 * 
	 * @author rmh3093
	 *
	 */
	@SuppressWarnings("deprecation")
	public void run() {
		while (true) {
			switch(model.stage) {

			case WAIT:
				synchronized (model.trialThread) {
					try {
						model.trialThread.wait();
					} catch (InterruptedException e1) {
					}
				}
				
			case START_TRIAL:
				startTrials();
				model.stage = Model.TrialStage.GENERATE_TARGETS;
				
			case GENERATE_TARGETS:
				generateTargets();
				model.stage = Model.TrialStage.TRIAL_SPLASH;
				
			case TRIAL_SPLASH:
				view.trial.drawTrialSplash();
				model.stage = Model.TrialStage.MOVE_TARGETS;
				
			case MOVE_TARGETS:
				if (model.duration < model.maxduration) {
					moveTargets();
					break;
				} else {
					model.stage = Model.TrialStage.CHOOSE_TARGET;
				}
				
			case CHOOSE_TARGET:
				view.trial.repaint();
				model.target = rand.nextInt(model.targets.size());
				model.stage = Model.TrialStage.START_SEARCH;
				
			case START_SEARCH:
				view.trial.setCursor(Cursor.CROSSHAIR_CURSOR);
				view.resetMouse();
				model.startTime = (int)System.currentTimeMillis();
				model.stage = Model.TrialStage.FIND_TARGET;
				
			case FIND_TARGET:
				view.trial.repaint();
				break;
				
			case STOP_SEARCH:
				model.stopTime = (int)System.currentTimeMillis();
				String text = "Trial";
				int rt = model.stopTime - model.startTime;
				if (view.cp.simmode_combobox.getSelectedIndex() > 0)
					text = text + " " + model.trialCount;
				if (model.participant != null)
					//model.participant.trialResults.add(rt);
				model.stage = Model.TrialStage.TRIAL_CLEANUP;
				
			case TRIAL_CLEANUP:
				if ((view.cp.simmode_combobox.getSelectedIndex()!=2) && 
						(model.trialCount >= model.trialMax)) {
					view.trial.stopTrial();
					if (model.participant != null)
						results.writeResultsXML(model.participant);
					model.stage = Model.TrialStage.WAIT;
				} else {
					model.trialCount++;
					view.trial.clearScreen();
					view.trial.setupTrial();
					model.stage = Model.TrialStage.GENERATE_TARGETS;
				}
				break;
			}
		}
	}

	void startTrials() {
		view.startTrial();
		model.target_font = new Font(view.trial.getFont().getName(),
				view.trial.getFont().getStyle(),
				(Integer)view.cp.fontsize_spinner.getValue());
		model.query_font = new Font(view.trial.getFont().getName(),
				view.trial.getFont().getStyle(), model.query_font_size);
		model.trial_splash_font = new Font(view.trial.getFont().getName(),
				view.trial.getFont().getStyle(), model.query_font_size*2);
		view.trial.bufferGraphics.setFont(model.target_font);
		model.duration = 0;
		view.trial.mh = model.query_font.getSize() + model.query_font_size;
		if (view.cp.trials_spinner.isEnabled())
			model.trialMax = 
				Integer.valueOf(view.cp.trials_spinner.getValue().toString());
	}

}
