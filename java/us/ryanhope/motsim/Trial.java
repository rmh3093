package us.ryanhope.motsim;
/**
 * Trial.java
 * motsim
 */

/*
Copyright (c) 2008, Ryan M. Hope
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
	  this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
 * Neither the name of the project nor the names of its contributors may be
	  used to endorse or promote products derived from this software without
	  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;

import javax.swing.JFrame;

@SuppressWarnings("serial")
class Trial extends JFrame {

	Graphics2D bufferGraphics;
	FontRenderContext frc;
	Image offscreen;
	int mh;
	Model model;
	Dimension screen_size;
	EventHandler evh;
	View view;
	
	public Trial(Model model, View view, Dimension screen_size, EventHandler evh) {
		this.model = model;
		this.view = view;
		this.screen_size = screen_size;
		this.evh = evh;
		addWindowListener(evh);
		addMouseListener(evh);
		addMouseMotionListener(evh);
		addKeyListener(evh);
		setPreferredSize(screen_size);
		setResizable(false);
		setLocationRelativeTo(null);
		setUndecorated(true);
		setAlwaysOnTop(true);
		model.gd.setFullScreenWindow(this);
		setBackground(Color.black);
		offscreen = createImage(screen_size.width, screen_size.height);
		bufferGraphics = (Graphics2D)offscreen.getGraphics();
		frc = bufferGraphics.getFontRenderContext();
		setupTrial();
	}
	
	public void setupTrial() {
		setCursor(view.blank_cursor);
		model.startTime = 0;
		model.stopTime = 0;
		model.duration = 0;
	}
	
	private void drawTarget(Target t, boolean isMasked) {
		bufferGraphics.setColor(model.target_color);
		TextLayout tl;
		String text;
		if (isMasked)
			text = "XXXXXX";
		else
			text = t.callsign;
		tl = new TextLayout(text, model.target_font, frc);
		t.bounds = tl.getBounds();
		tl.draw(bufferGraphics, (float)t.x, (float)t.y);
	}
	
	private void processTargets() {
		
		Target t;
		boolean isMasked = false;
		
		for (int i=0; i<model.targets.size(); i++) {

			t = model.targets.get(i);
			
			if (model.stage == Model.TrialStage.FIND_TARGET) {
				if ((model.mouse_x>=t.x) &&
						(model.mouse_x<=(t.x+t.bounds.getWidth())) &&
						(model.mouse_y>=(t.y-t.bounds.getHeight())) &&
						(model.mouse_y<=t.y) )
					isMasked = false;
				else
					isMasked = true;
			}
			
			if ((model.mouse_x_clk>=t.x) &&
					(model.mouse_x_clk<=(t.x+t.bounds.getWidth())) &&
					(model.mouse_y_clk>=(t.y-t.bounds.getHeight())) &&
					(model.mouse_y_clk<=t.y) ) {
				if (model.targets.get(model.target)==t) {
					model.stage = Model.TrialStage.STOP_SEARCH;
				}
			}
			
			drawTarget(t, isMasked);
				
		}
		
	}

	public void drawFrame() {
		bufferGraphics.setColor(model.target_color);
		bufferGraphics.drawRect(0, 0, screen_size.width-1, 
				screen_size.height-1);
		bufferGraphics.drawRect(0, screen_size.height-1-mh, 
				screen_size.width-1, screen_size.height-1);
	}

	public void drawQuery() {
		bufferGraphics.setColor(model.query_color);
		TextLayout tl = new TextLayout("Click on target: " + "\"" + 
				model.targets.get(model.target).callsign + "\"", 
				model.query_font, frc);
		tl.draw(bufferGraphics, 
				(int)Math.round(
						screen_size.width / 2 - tl.getBounds().getCenterX()
				),
				(int)Math.round(
						screen_size.height - mh / 2 +
						tl.getBounds().getCenterY() / 2
				)
		);
		bufferGraphics.setColor(model.target_color);
	}
	
	public void drawTrialSplash() {
		bufferGraphics.setColor(model.query_color);
		String splash;
		if (view.cp.simmode_combobox.getSelectedIndex() > 0)
			splash = "Trial " + model.trialCount;
		else
			splash = "Single Trial";
		TextLayout tl = new TextLayout(splash, 
				model.trial_splash_font, frc);
		tl.draw(bufferGraphics, 
				(int)Math.round(screen_size.width/2 - tl.getBounds().getWidth()/2), 
				(int)Math.round(screen_size.height/2)
				);
		repaint();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		if ((model.stage.ordinal() == Model.TrialStage.MOVE_TARGETS.ordinal()) || 
				model.stage.ordinal() == Model.TrialStage.FIND_TARGET.ordinal()
				) {
			clearScreen();
			drawFrame();
			if (model.stage == Model.TrialStage.FIND_TARGET)
				drawQuery();
			processTargets();
		}
		g2.drawImage(offscreen, 0, 0, this);
	}

	public void update(Graphics g) {
		paint(g);
	}

	public void clearScreen() {
		bufferGraphics.clearRect(0, 0, screen_size.width, 
				screen_size.height);
	}
	
	protected void stopTrial() {
		setVisible(false);
	}

}
