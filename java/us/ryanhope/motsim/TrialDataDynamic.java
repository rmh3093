package us.ryanhope.motsim;
/**
 * TrialDataDynamic.java
 * motsim
 */

/*
Copyright (c) 2008, Ryan M. Hope
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
	  this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
 * Neither the name of the project nor the names of its contributors may be
	  used to endorse or promote products derived from this software without
	  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

public class TrialDataDynamic {

	Random rand;
	Dimension screen_size;

	/**
	 * An array of aircraft operator prefixes.
	 */
	final private String[] callsign_prefixes = {
			"SWA", "AAL", "UAL", "COA", "DAL", "FDX", "NWA", "UPS", "SKW", 
			"ACA", "BTA", "AWE", "JBU", "USA", "ASA", "TRS", "BAW", "ABX", 
			"EGF", "DLH", "WJA", "AFR", "ASH", "ASQ", "AMF", "FFT", "FLG", 
			"COM", "JZA", "CHQ", "QXE", "KLM", "DHL", "MES", "HAL", "USC", 
			"LOF", "VIR", "CJC", "KAL", "JIA", "VRD", "NKS", "TSC", "RPA", 
			"TCF", "CPA", "AWI", "IBE", "MXA", "EJA", "AZA", "SWR", "MEP", 
			"PEN", "LBQ", "GLA", "EIN", "CAL", "AAY", "AMX", "TCX", "MPH", 
			"CCI", "FLX", "BKA", "JAZ", "AIC", "GJS", "JAL", "ANA", "SCX", 
			"MTN", "FTA", "PDT", "LTU", "CPZ", "REX", "AJM", "MPE", "SIA", 
			"ICE", "SAS", "AEA", "CRL", "SKQ", "CMI", "CLX", "FIV", "TAI", 
			"MRA", "CJT", "ELY", "EVA", "NCB", "PCO", "ATN", "TOM", "CFG", 
			"AAR", "LAN", "MAL", "GTI", "JAI", "ERH", "CMP", "AUA", "RZO", 
			"GSM", "SWG", "AVA", "THT", "WAE", "SNC", "WIG", "LOT", "CCA", 
			"AFL", "XLA", "FRL", "GLR", "TAM", "FCA", "UAE", "GWY", "SVX", 
			"MDS", "SAA", "CWC", "BMA", "CAV", "DCS", "LAL", "OOM", "MUI", 
			"OAL", "KFA", "LYM", "WEW", "CTA", "BBR", "HKI", "MKU", "IWD", 
			"WIS", "RAM", "CPT", "CGC", "SUB", "FAB", "FIN", "CUB", "VTS", 
			"CGL", "CLA", "WAV", "ADH", "RJA", "AAH", "CKK", "SSX", "ANZ", 
			"WOA", "TAP", "THU", "VPB", "LNX", "CES", "KAP", "LPE", "GGN", 
			"NWD", "EXA", "WWI", "CHH", "PCE", "CAP", "SLI", "APW", "ACT", 
			"VPA", "CAO", "THY", "WIA", "CNK", "CGR", "CFN", "FPG", "APC", 
			"FWL", "GRC", "TFL", "UKZ", "NMI", "PRO", "ASP", "CON", "EMD", 
			"ETH", "SSV", "GCO", "AJT", "DNJ", "FRE", "BXR", "TAG", "BVN", 
			"BOS", "CKS", "PRY", "PAC", "PGX", "PAG", "NOA", "JDC", "AIP", 
			"BLS", "GLT", "MAH", "BXH", "CFC", "SQC", "VAL", "MAS", "ARG", 
			"MAX", "CGN", "NRL", "LNE", "ETD", "GUN", "OAE", "QTR", "EJM", 
			"ANT", "NDU", "ISR", "TWY", "MSR", "CCY", "OPT", "TEC", "PWA", 
			"LRC", "BOE", "MPD", "QFA", "CFZ", "CJA", "AVI", "CSA", "GEC", 
			"QUE"
	};

	public TrialDataDynamic(Dimension screen_size) {
		rand = new Random();
		this.screen_size = screen_size;
	}

	/**
	 * Generate a set of random callsigns using a random prefix from
	 * "callsign_prefixes" and a random 3 digit number.
	 * 
	 * @param count The number of callsigns to generate.
	 * @return A HashSet of callsigns.
	 */
	private HashSet<String> generate_callsigns(int count) {
		
		HashSet<String> callsigns = new HashSet<String>(count);
		
		while (callsigns.size() < count) {
			int randomInt = rand.nextInt(callsign_prefixes.length);
			int callsign_suffix = 0;
			while (callsign_suffix < 100) {
				callsign_suffix = rand.nextInt(1000);
			}
			callsigns.add(callsign_prefixes[randomInt] + 
					String.valueOf(callsign_suffix));
		}
		
		return callsigns;
		
	}

	/**
	 * Generate a set of random coordinates
	 * 
	 * @param count The number of coordinates to generate.
	 * @return A HashSet of coordinates.
	 */
	private HashSet<Point2D.Double> generate_coordinates(int count) {
		
		HashSet<Point2D.Double> coordinates = 
			new HashSet<Point2D.Double>(count);
		
		while (coordinates.size() < count) {		
			double x = rand.nextInt(screen_size.width);
			double y = rand.nextInt(screen_size.height);
			Point2D.Double point = new Point2D.Double();
			point.setLocation(x, y);
			coordinates.add(point);
		}
		
		return coordinates;
		
	}
	
	/**
	 * Generate a random set of targets.
	 * 
	 * @param count The number of callsigns to generate.
	 * @return A HashSet of coordinates.
	 */
	protected HashSet<Target> generate_targets( int count ) {
		
		HashSet<Target> targets = new HashSet<Target>(count);
		HashSet<String> callsigns = generate_callsigns(count);
		HashSet<Point2D.Double> coordinates = generate_coordinates(count);
		Iterator<String> callsignsIter = callsigns.iterator();
		Iterator<Point2D.Double> coordinatesIter = coordinates.iterator();
		
		while ((targets.size() < count) && 
				(callsignsIter.hasNext() && callsignsIter.hasNext())) {
			int angle = rand.nextInt(360);
			String callsign = callsignsIter.next();
			Point2D.Double coordinate = coordinatesIter.next();
			double xv = calcAngleMoveX(angle);
			double yv = calcAngleMoveY(angle);
			Target target = 
				new Target(callsign, coordinate.x, coordinate.y, 
						angle, xv, yv, 1);
			targets.add(target);
		}
		
		return targets;
		
	}
	
	protected void spawnTargetOnBorder(ArrayList<Target> targets_array, 
			int count) {
		
		HashSet<Target> targets = new HashSet<Target>(targets_array);
		HashSet<String> callsigns;
		HashSet<Point2D.Double> coordinates;
		Iterator<String> callsignsIter;
		Iterator<Point2D.Double> coordinatesIter;
		Target target;
		
		int size = targets.size() + count;
		
		while ( targets.size() < size ) {
			callsigns = generate_callsigns(1);
			coordinates = generate_coordinates(1);
			callsignsIter = callsigns.iterator();
			coordinatesIter = coordinates.iterator();
			int angle = rand.nextInt(360);
			String callsign = callsignsIter.next();
			Point2D.Double coordinate = coordinatesIter.next();
			double xv = calcAngleMoveX(angle);
			double yv = calcAngleMoveY(angle);
			int border = rand.nextInt(4);
			if (border == 0) {
				coordinate.x = 0;
			} else if (border == 1) {
				coordinate.x = screen_size.width;
			} else if (border == 2) {
				coordinate.y = 0;
			} else if (border == 3) {
				coordinate.y = screen_size.height;
			}
			target = 
				new Target(callsign, coordinate.x, coordinate.y, 
						angle, xv, yv, 1);
			targets.add(target);
			if (targets.contains(target))
				targets_array.add(target);
		}
		
	}
	
	public double calcAngleMoveX(double angle) {
		return (double) (Math.cos(angle * Math.PI / 180));
	}

	public double calcAngleMoveY(double angle) {
		return (double) (Math.sin(angle * Math.PI / 180));
	}
	
	public int[] generateEntropyLow() {
		int[] low = new int[10000];
		for (int i = 0; i < 10000; i++) { 
			if (i<8000) {
				low[i] = 0;
			} else if(i<8300) {
				low[i] = 5;
			} else if(i<8600) {
				low[i] = -5;
			} else if(i<8800) {
				low[i] = 10;
			} else if(i<9000) {
				low[i] = -10;
			} else if(i<9100) {
				low[i] = 15;
			} else if(i<9200) {
				low[i] = -15;
			} else if(i<9200) {
				low[i] = -15;
			} else if(i<9300) {
				low[i] = 20;
			} else if(i<9400) {
				low[i] = -20;
			} else if(i<9500) {
				low[i] = 25;
			} else if(i<9600) {
				low[i] = -25;
			} else if(i<9650) {
				low[i] = 30;
			} else if(i<9700) {
				low[i] = -30;
			} else if(i<9750) {
				low[i] = 35;
			} else if(i<9800) {
				low[i] = -35;
			} else if(i<9850) {
				low[i] = 40;
			} else if(i<9900) {
				low[i] = -40;
			} else if(i<9950) {
				low[i] = 45;
			} else if(i<10000) {
				low[i] = -45;
			}
		}
		return low;
	}

	public int[] generateEntropyMedium() {
		int[] medium = new int[10000];
		for (int i = 0; i < 10000; i++) { 
			if (i<4000) {
				medium[i] = 0;
			} else if (i<5250) {
				medium[i] = 5;
			} else if (i<6500) {
				medium[i] = -5;
			} else if (i<7300) {
				medium[i] = 10;
			} else if (i<8100) {
				medium[i] = -10;
			} else if (i<8600) {
				medium[i] = 15;
			} else if (i<9100) {
				medium[i] = -15;
			} else if (i<9200) {
				medium[i] = 20;
			} else if (i<9300) {
				medium[i] = -20;
			} else if (i<9400) {
				medium[i] = 25;
			} else if (i<9500) {
				medium[i] = -25;
			} else if (i<9600) {
				medium[i] = 30;
			} else if (i<9700) {
				medium[i] = -30;
			} else if (i<9750) {
				medium[i] = 35;
			} else if (i<9800) {
				medium[i] = -35;
			} else if (i<9850) {
				medium[i] = 40;
			} else if (i<9900) {
				medium[i] = -40;
			} else if (i<9950) {
				medium[i] = 45;
			} else if (i<10000) {
				medium[i] = -45;
			}
		}
		return medium;
	}

	public int[] generateEntropyHigh() {
		int[] high = new int[10000];
		for (int i = 0; i < 10000; i++) { 
			if (i<526) {
				high[i] = -45;
			} else if (i<1052) {
				high[i] = 45;
			} else if (i<1578) {
				high[i] = -40;
			} else if (i<2104) {
				high[i] = 40;
			} else if (i<2630) {
				high[i] = -35;
			} else if (i<3156) {
				high[i] = 35;
			} else if (i<3682) {
				high[i] = -30;
			} else if (i<4208) {
				high[i] = 30;
			} else if (i<4734) {
				high[i] = -25;
			} else if (i<5260) {
				high[i] = 25;
			} else if (i<5786) {
				high[i] = -20;
			} else if (i<6312) {
				high[i] = 20;
			} else if (i<6838) {
				high[i] = -15;
			} else if (i<7364) {
				high[i] = 15;
			} else if (i<7890) {
				high[i] = -10;
			} else if (i<8416) {
				high[i] = 10;
			} else if (i<8942) {
				high[i] = -5;
			} else if (i<9468) {
				high[i] = 5;
			} else if (i<10000) {
				high[i] = 0;
			}
		}
		return high;
	}

}
