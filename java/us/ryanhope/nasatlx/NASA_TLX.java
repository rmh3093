/**
 * NASA_TLX.java
 */

/**
 * NASA-TLX is a subjective workload assessment tool. 
 * 
 * NASA-TLX allows users to perform subjective workload assessments on 
 * operator(s) working with various human-machine systems. NASA-TLX is a 
 * multi-dimensional rating procedure that derives an overall workload score 
 * based on a weighted average of ratings on six subscales.
 * 
 * These subscales include Mental Demands, Physical Demands, Temporal Demands, 
 * Own Performance, Effort and Frustration. It can be used to assess workload 
 * in various human-machine environments such as aircraft cockpits, command, 
 * control, and communication (C3) workstations; supervisory and process 
 * control environments; simulations and laboratory tests.
 * 
 * http://humansystems.arc.nasa.gov/groups/TLX/downloads/NASA-TLXChapter.pdf
 * 
 * Hart, S.G. & Staveland, L.E. (1988). Development of NASA-TLX 
 * 		(Task Load Index): Results of empirical and theoretical research. In 
 * 		 P.A. Hancock & N. Meshkati (Eds.), Human Mental Workload (pp. 239-250). 
 * 		 Amsterdam: North Holland Press.
 * 
 * 
 * @author Ryan M. Hope <rmh3093@gmail.com>
 *
 */

package us.ryanhope.nasatlx;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSlider;
import javax.swing.WindowConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import us.ryanhope.experiment.ParticipantGUI;
import us.ryanhope.experiment.Survey;
import us.ryanhope.experiment.SurveyQuestion;
import us.ryanhope.util.CombinationGenerator;

import jargs.gnu.CmdLineParser;

@SuppressWarnings("unchecked")
public class NASA_TLX implements ActionListener {

	public static String[] default_subscales = {
		"Mental Demand",
		"Physical Demand",
		"Temporal Demand",
		"Own Performance",
		"Effort",
		"Frustration"
	};

	SystemTray systray;
	TrayIcon trayIcon;
	JPopupMenu popupmenu;
	int tasks = 0;
	boolean verbose = false;
	Dimension screen_size;

	/*public int scoreTask(TreeMap<String, String> taskRating) {
		int score = 0;
		int sum = 0;
		for (String s : subscales) {
			int r = Integer.valueOf(taskRating.get(s)); 
			int w = Integer.valueOf(biasTally.get(s));
			sum += w;
			score += (r * w);
		} 
		return score / sum;
	}*/

	/*public void printBiasTally() {
		if (biasTally!=null) {
			for (String s : subscales)
				System.out.println(s + " = " + biasTally.get(s));
		}
	}

	public TreeMap<String, String> getBiasTally() {
		TreeMap<String, String> bt = new TreeMap<String, String>();
		if (biasTally!=null) {
			for (String s : subscales)
				bt.put(s, String.valueOf(biasTally.get(s)));
		}
		return bt;
	}*/

	/*public void printTaskRatings() {
		if (taskRatings!=null) {
			for (String t : taskRatings.keySet()) {
				TreeMap<String, String> taskRating = taskRatings.get(t);
				for (String s : subscales) {
					System.out.println(s + " = " + taskRating.get(s));
				}
			}
		}
	}*/

	/*public TreeMap<String, TreeMap<String, String>> getTaskRatings() {
		return taskRatings;
	}*/

	public static Integer[][] generateComparisons(String[] elements) {
		int[] indices;
		Vector<Integer[]> pairs = new Vector<Integer[]>();
		CombinationGenerator cg = 
			new CombinationGenerator(elements.length, 2);
		while (cg.hasMore()) {
			indices = cg.getNext();
			Integer[] pair = {indices[0],indices[1]};
			pairs.add(pair);
		}
		Integer[][] subscale_pairs = new Integer[pairs.size()][];
		for (int i=0; i<pairs.size(); i++) {
			Integer[] p = pairs.get(i);
			subscale_pairs[i] = p;
		}
		return subscale_pairs;	
	}

	@SuppressWarnings("serial")
	public class TaskRating extends Observable {

		public Hashtable<Integer, JLabel> labelTable;
		public TRPanel[] panels;
		public JPanel left, right;
		public JButton button;
		public JFrame main;
		public String[] subscales = null;
		
		class TRPanel extends JPanel {
			JSlider tlx_slider;
			String title;
			public TRPanel(String subscale) {
				title = subscale;
				setBorder(
						BorderFactory.createTitledBorder(
								new CompoundBorder(
										BorderFactory.createTitledBorder(
												BorderFactory.createEtchedBorder(),
												title, TitledBorder.CENTER, 
												TitledBorder.CENTER), 
												new EmptyBorder(10,20,10,20))));
				setLayout(new GridLayout(2,1));
				tlx_slider = new JSlider(0,100,50);
				tlx_slider.setMajorTickSpacing(10);
				tlx_slider.setLabelTable(labelTable);
				tlx_slider.setPaintTicks(true);
				tlx_slider.setPaintLabels(true);
				add(tlx_slider);
			}
		}

		public TaskRating(boolean fullscreen, Observer o) {
			if (o!=null)
				addObserver(o);
			subscales = NASA_TLX.default_subscales;
			main = new JFrame("Task Rating");
			left = new JPanel(new GridLayout(0,2,10,10));
			right = new JPanel(new FlowLayout());
			labelTable = new Hashtable<Integer, JLabel>();
			labelTable.put( new Integer(0), new JLabel("Min") );
			labelTable.put( new Integer(100), new JLabel("Max") );
			panels = new TRPanel[subscales.length];
			for (int i=0; i<subscales.length; i++) {
				panels[i] = new TRPanel(subscales[i]);
				left.add(panels[i]);
			}
			button = new JButton("Done");
			ActionListener l = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					TreeMap<String, String> taskRating = 
						new TreeMap<String, String>();
					for (int i=0; i<panels.length; i++) {
						taskRating.put(panels[i].title, String.valueOf(panels[i].tlx_slider.getValue()));
					}
					setChanged();
					notifyObservers(taskRating);
				};
			};
			button.addActionListener(l);
			right.add(button);
			main.add(left);
			main.add(right);
			main.setResizable(false);
			FlowLayout fl = new FlowLayout();
			if (fullscreen) {
				main.setPreferredSize(screen_size);
				main.setUndecorated(true);
				main.setAlwaysOnTop(true);
				fl.setVgap(screen_size.height/4);
			} else {
				main.setLocationRelativeTo(null);
			}
			main.setLayout(fl);
			main.pack();
			main.setVisible(true);
		}

	}

	@SuppressWarnings("serial")
	public class PairWiseComparison extends Observable {

		public JFrame main = null;
		public int pair = 0;
		public PWCPanel[] panels= null;
		public TreeMap<String, String> biasTally = null;
		public Integer[][] subscale_pairs = null;
		public String[] subscales = null;

		public class PWCPanel extends JPanel {

			JButton b1,b2;

			public PWCPanel(String subscale1, String subscale2, ActionListener al) {
				setLayout(new GridLayout(2,1,10,10));
				b1 = new JButton(subscale1);
				b1.setActionCommand(subscale1);
				b1.addActionListener(al);
				b1.setFocusable(false);
				b1.setPreferredSize(new Dimension(220,50));
				b2 = new JButton(subscale2);
				b2.setActionCommand(subscale2);
				b2.addActionListener(al);
				b2.setFocusable(false);
				b2.setPreferredSize(new Dimension(220,50));
				add(b1);
				add(b2);
				setVisible(true);
			}

		}

		/**
		 * Participant selects the member of each pair that provides the most
		 * significant source of workload variation in the tasks
		 * 
		 * @param verbose If true the results will be printed to stdout.
		 */
		public PairWiseComparison (boolean fullscreen, String[] subscales, Observer o) {
			this.subscales = subscales;
			if (o!=null)
				addObserver(o);
			biasTally = new TreeMap<String, String>();
			for (String s : subscales)
				biasTally.put(s, String.valueOf(0));
			subscale_pairs = generateComparisons(subscales);
			main = new JFrame("Pair-wise Comparison");
			FlowLayout fl = new FlowLayout();
			if (fullscreen) {
				main.setPreferredSize(screen_size);
				main.setUndecorated(true);
				main.setAlwaysOnTop(true);
				fl.setVgap(screen_size.height/2-60);
			} else {
				main.setLocationRelativeTo(null);
				main.setResizable(false);
				main.setPreferredSize(new Dimension(250,120));
			}
			main.setLayout(fl);
			main.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			panels = new PWCPanel[subscale_pairs.length];
			ActionListener l = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int oldval = Integer.valueOf(biasTally.get(e.getActionCommand()));
					biasTally.put(e.getActionCommand(), String.valueOf(oldval+1));
					panels[pair].setVisible(false);
					pair++;
					if (pair<subscale_pairs.length) {
						main.remove(panels[pair-1]);
						main.add(panels[pair]);
					} else {
						setChanged();
						notifyObservers(biasTally);
					}
				}
			};
			for (int i=0; i<subscale_pairs.length; i++) {
				panels[i] = new PWCPanel(subscales[subscale_pairs[i][0]], 
						subscales[subscale_pairs[i][1]],l);
				panels[i].setVisible(true);
			}
			main.add(panels[pair]);
			main.pack();
			main.setVisible(true);
		}

	}

	private void printUsage() {
		System.err.println("Usage: NASA_TLX [OPTIONS]\n");
		System.err.println("Option\t\tGNU long option\t\tMeaning");
		System.err.println("-v\t\t--verbose\t\tEnable verbose output");
		System.err.println("-t\t\t--tasks number\t\tThe number of subtasks");
		System.err.println("-d\t\t--disable subscale\tDisable a subscale");
		System.err.println("-s\t\t--systray\t\tRun in systray");
		System.err.println("\nAvailable subscales:");
		for (int i=0; i<default_subscales.length; i++)
			System.err.println("  " + (i+1) + ". " + default_subscales[i]);
	}

	@SuppressWarnings("serial")
	private void setupSystray() {
		systray = SystemTray.getSystemTray();
		Image image = Toolkit.getDefaultToolkit().getImage("us/ryanhope/" +
		"nasatlx/nasa-large.png").getScaledInstance(24, 24, 
				Image.SCALE_SMOOTH);
		popupmenu = new JPopupMenu("NASA-TLX");
		popupmenu.add(new JMenuItem("New Participant") {{
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Vector<SurveyQuestion> questions = new Vector<SurveyQuestion>();
					questions.add(new SurveyQuestion(Survey.QuestionType.TEXT, "Full Name", null));
					ParticipantGUI pg = new ParticipantGUI(WindowConstants.DISPOSE_ON_CLOSE);
					ActionListener l = new ActionListener() {
						public void actionPerformed(ActionEvent e) {				
						}
					};
					pg.newSurvey("New Participant", questions, l);
				}
			}
			);
		}});
		popupmenu.addSeparator();
		popupmenu.add(new JMenuItem("Bias Rating") {{
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new PairWiseComparison(false, default_subscales,null);
				}
			}
			);
		}});
		popupmenu.add(new JMenuItem("Task Rating") {{
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new TaskRating(false, null);
				}
			}
			);
		}});
		popupmenu.addSeparator();
		popupmenu.add(new JMenuItem("Exit") {{
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			}
			);
		}});
		try {
			trayIcon = new TrayIcon(image, "NASA-TLX",null);
			trayIcon.addMouseListener(new MouseAdapter() {
				public void mouseReleased(MouseEvent e) {
					popupmenu.setVisible(false);
					if(e.isPopupTrigger()) {
						popupmenu.setInvoker(popupmenu);
						popupmenu.setLocation(e.getX(), e.getY());
						popupmenu.setVisible(true);
					}
				}
				public void mousePressed(MouseEvent e) {
					mouseReleased(e);
				}
			});
			systray.add(trayIcon);			
		} catch (AWTException e) {
		}
	}

	static public void daemonize() {
		System.out.close();
		System.err.close();
	}

	private void commonSetup() {
		screen_size = Toolkit.getDefaultToolkit().getScreenSize();
		//taskRatings = new TreeMap<String, TreeMap<String, String>>();
		//subscale_pairs = generateComparisons(subscales);
	}

	public NASA_TLX() {
		commonSetup();
	}

	public NASA_TLX(String[] args) {

		CmdLineParser parser = new CmdLineParser();
		CmdLineParser.Option tasks = parser.addIntegerOption('t', "tasks");
		CmdLineParser.Option disable = parser.addIntegerOption('d', "disable");
		CmdLineParser.Option systray = parser.addBooleanOption('s', "systray");
		CmdLineParser.Option verbose = parser.addBooleanOption('v', "verbose");
		CmdLineParser.Option help = parser.addBooleanOption('h', "help");

		try {
			parser.parse(args);
		}
		catch ( CmdLineParser.OptionException e ) {
			System.err.println(e.getMessage());
			printUsage();
			System.exit(2);
		}

		if ((Boolean)parser.getOptionValue(help, false)) {
			printUsage();
			System.exit(2);
		}

		this.tasks = (Integer)parser.getOptionValue(tasks, 1);
		this.verbose = (Boolean)parser.getOptionValue(verbose, false);
		Vector<Integer> disabled_subscales = parser.getOptionValues(disable);
		String[] subscales;
		if (disabled_subscales.isEmpty())
			subscales = default_subscales;
		else {
			subscales = 
				new String[default_subscales.length-disabled_subscales.size()];
			boolean found;
			int index = 0;
			for (int i=0; i<default_subscales.length; i++) {
				found = false;
				int idx = 0;
				while (!found && idx<disabled_subscales.size()) {
					if ((disabled_subscales.get(idx)-1)==i) found = true;
					else idx++;
				}
				if (!found) {
					subscales[index] = default_subscales[i];
					index++;
				}
			}
		}			
		commonSetup();

		if (SystemTray.isSupported() && 
				(Boolean)parser.getOptionValue(systray, false)) {
			//daemonize();
			setupSystray();
		} else {
			new PairWiseComparison(false, default_subscales, null);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {		
		new NASA_TLX(args);
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}
