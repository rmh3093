package us.ryanhope.usability;

import java.awt.Toolkit;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import org.apache.xmlrpc.XmlRpcException;
import org.eclipse.swt.*;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.browser.*;

import us.ryanhope.drupal.DrupalClient;
import us.ryanhope.experiment.Participant;
import us.ryanhope.experiment.ParticipantGUI;
import us.ryanhope.experiment.ParticipantResult;
import us.ryanhope.experiment.Survey;
import us.ryanhope.experiment.SurveyQuestion;
import us.ryanhope.nasatlx.NASA_TLX;
import us.ryanhope.nasatlx.NASA_TLX.PairWiseComparison;
import us.ryanhope.nasatlx.NASA_TLX.TaskRating;

@SuppressWarnings("unchecked")
public class OffbeatGuides implements Observer {

	final String EXPERIMENT_ID = "obgut";
	final String EXPERIMENT_NAME = "OffbeatGuides.com Usability Test Investigators";
	final int EXPERIMENT_GID = 74;
	final String XMLRPC_URL = "http://ryanhope.us/services/xmlrpc";
	final String username = "obgut";
	final String password = "400474970";
	final String BACKGROUNDINFO = "Background Information";

	Shell shell = null;
	Browser browser = null;
	int time = 0;
	int taskcount = 0;
	Map<String, String> times = null;
	Display display = null;
	Participant participant = null;
	Map<String, Vector<SurveyQuestion>> background_survey = null;
	Map<String, Vector<SurveyQuestion>> usability_survey = null;

	private void setupSurvey() {
		Vector<SurveyQuestion> questions;
		String[] choices;

		background_survey = new TreeMap<String, Vector<SurveyQuestion>>();
		questions = new Vector<SurveyQuestion>();
		choices = new String[] {"", "Strongly Disagree", "Disagree", "Neither Agree Nor Disagree", "Agree", "Strongly Agree"};
		questions.add(new SurveyQuestion(Survey.QuestionType.FIXED_CHOICE, "You are an experienced Internet user.", choices));
		choices = new String[] {"", "More than once a week", "More than once a month", "More than once a year", "Less than once a year", "Never"};
		questions.add(new SurveyQuestion(Survey.QuestionType.FIXED_CHOICE, "How frequently do you travel?", choices));
		questions.add(new SurveyQuestion(Survey.QuestionType.FIXED_CHOICE, "How frequently do you use online travel services (i.e. Expedia, Travelocity, etc.)?", choices));
		questions.add(new SurveyQuestion(Survey.QuestionType.FIXED_CHOICE, "How frequently do you use travel guides (i.e. Lonelyplanet, Fodor's, etc.)?", choices));
		background_survey.put(BACKGROUNDINFO, questions);

		usability_survey = new TreeMap<String, Vector<SurveyQuestion>>();

		questions = new Vector<SurveyQuestion>();
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are the results of control entry compatible with user expectations?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the control matched to user skill?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are the coding compatible with familiar conventions?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the wording familiar?", null));
		usability_survey.put("COMPATIBILITY", questions);

		questions = new Vector<SurveyQuestion>();
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the assignment of colour codes conventional?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the coding consistent across displays, menu options?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the feedback consistent?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the format within data fields consistent?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the label format consistent?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the label location consistent?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the display format consistent?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the display orientation consistent? -- panning vs. scrolling.", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are the user actions required consistent?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the wording consistent across displays?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the data display consistent with entry requirements?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the data display consistent with user conventions?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are symbols for graphic data standard?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the option wording consistent with command language?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the wording consistent with user guidance?", null));
		usability_survey.put("CONSISTENCY", questions);

		questions = new Vector<SurveyQuestion>();
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the design for data entry flexible?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide flexible sequence control?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide flexible user guidance?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are the menu options dependent on context?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Can user name displays and elements according to their needs?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide good training for different users?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide user selection of data for display?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it handle user-specified windows?", null));
		usability_survey.put("FLEXIBILITY", questions);

		questions = new Vector<SurveyQuestion>();
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide clarity of wording?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the data grouping reasonable for easy learning?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the grouping of menu options logical?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the ordering of menu options logical?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are the command names meaningful?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide no-penalty learning?", null));
		usability_survey.put("LEARNABILITY", questions);

		questions = new Vector<SurveyQuestion>();
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide combined entry of related data?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Will the required data be entered only once?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide default values?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide function keys for frequent control entries?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide global search and replace capability?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the menu selection by pointing? -- primary means of sequence control.", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the menu selection by keyed entry? -- secondary means of control entry.", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it require minimal cursor positioning?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it require minimal steps in sequential menu selection?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it require minimal user control actions?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the return to higher-level menus required only one simple key action?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the return to general menu required only one simple key action?", null));
		usability_survey.put("MINIMAL ACTION", questions);

		questions = new Vector<SurveyQuestion>();
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "How are abbreviations and acronyms used?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide aids for entering hierarchic data?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the guidance information always available?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide hierarchic menus for sequential selection?", null)); 
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are selected data highlighted?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide index of data?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it indicate current position in menu structure?", null)); 
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are data items kept short?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are the letter codes for menu selection designed carefully?", null)); 
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are long data items partitioned?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are prior answers recapitulated?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are upper and lower case equivalent?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it use short codes rather than long ones?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide supplementary verbal labels for icons?", null));
		usability_survey.put("MINIMAL MEMORY LOAD", questions);

		questions = new Vector<SurveyQuestion>();
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide coding by data category?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the abbreviation distinctive?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are display elements distinctive?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the format for user guidance distinctive?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the spelling distinctive for commands?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide easily distinguished colours?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are items paired for direct comparison?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the number of spoken messages limited?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide lists for related items?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are menus distinct from other displayed information?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the colour coding redundant?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide visually distinctive data fields?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the screen density reasonable?", null));
		usability_survey.put("PERCEPTUAL LIMITATION", questions);

		questions = new Vector<SurveyQuestion>();
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "System feedback: How helpful is the error message?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide CANCEL option?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are erroneous entries displayed?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide explicit entry of corrections?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide feedback for control entries?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is HELP provided?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is completion of processing indicated?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are repeated errors indicated?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Are error messages non-disruptive/informative?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide RESTART option?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Does it provide UNDO to reverse control actions?", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.LIKERT1, "Is the sequence control user initiated?", null));
		usability_survey.put("USER GUIDANCE", questions);

	};

	public OffbeatGuides() {

		setupSurvey();

		newParticipant();
		newBackgroundInformation();
		newPairwiseComparison();

		times = new TreeMap<String, String>();
		display = new Display();
		shell = new Shell(display);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		shell.setLayout(gridLayout);
		Point p = new Point(Toolkit.getDefaultToolkit().getScreenSize().width, 
				Toolkit.getDefaultToolkit().getScreenSize().height);
		shell.setSize(p);
		
		Vector<Object[]> buttonText = new Vector<Object[]>();
		buttonText.add(new Object[]{SWT.PUSH,"Start Time"});
		buttonText.add(new Object[]{SWT.PUSH,"Stop Time"});
		buttonText.add(new Object[]{SWT.SEPARATOR,20});
		buttonText.add(new Object[]{SWT.PUSH,"Task Rating"});
		buttonText.add(new Object[]{SWT.SEPARATOR,20});
		buttonText.add(new Object[]{SWT.PUSH,"Reset"});
		buttonText.add(new Object[]{SWT.SEPARATOR,20});
		buttonText.add(new Object[]{SWT.PUSH,"Done"});
		
		GridData data = new GridData();
		data.horizontalAlignment = GridData.FILL;
		data.grabExcessHorizontalSpace = true;
		CoolBar controls = new CoolBar(shell, SWT.BORDER);
		controls.setLayoutData(data);
		controls.setLocked(true);
		Vector<Button> buttons = new Vector<Button>();
		for (Object[] o : buttonText) {			
			if ((Integer)o[0]==SWT.SEPARATOR) {
				CoolItem item = new CoolItem(controls, SWT.SEPARATOR);
				item.setSize((Integer)o[1], SWT.DEFAULT);
			} else {
				CoolItem item = new CoolItem(controls, SWT.NONE);
				Button button = new Button(controls, (Integer)o[0]);
				button.setText((String)o[1]);
				Point size = button.computeSize(SWT.DEFAULT, SWT.DEFAULT);
				item.setPreferredSize(item.computeSize(size.x, size.y));
				item.setControl(button);
				buttons.add(button);
			}
		}
		controls.pack();
		
		Listener listener = new Listener() {
			public void handleEvent(Event event) {
				Button button = (Button)event.widget;
				String string = button.getText();
				if (string.equals("Start Time")) {
					setStartTime();
				} else if (string.equals("Stop Time")) {
					calculateTime();			
				} else if (string.equals("Task Rating")) {
					newTaskRating();
				} else if (string.equals("Done")) {
					shell.dispose();
				}
			}
		};
		
		for (Button b : buttons) {
			b.addListener(SWT.Selection, listener);
		}
	
		try {
			browser = new Browser(shell, SWT.NONE);
		} catch (SWTError e) {
			System.out.println("Could not instantiate Browser: " + e.getMessage());
			return;
		}

		data = new GridData();
		data.horizontalAlignment = GridData.FILL;
		data.verticalAlignment = GridData.FILL;
		data.horizontalSpan = 1;
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = true;
		browser.setLayoutData(data);

		shell.open();
		showOBG();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
		
		newUsabilitySurvey();
		
		saveData();
	}

	private void printError(Exception e) {
		JOptionPane.showMessageDialog(null, "<html>"+e.getMessage()+"</html>", 
				null, JOptionPane.WARNING_MESSAGE);
	}

	public void saveData() {
		try {
			//Map<String, Map<String, String>> data = getResults();
			Map<String, Map<String, String>> data = 
				new HashMap<String, Map<String,String>>();
			for (String key : data.keySet())
				participant.results.put(key, data.get(key));
			DrupalClient d = new DrupalClient(XMLRPC_URL);
			d.login(username, password);
			Map node = new TreeMap();
			//node.put("status", 0);
			node.put("promote", false);
			String title = EXPERIMENT_ID+"["+participant.pid+"]";
			node.put("title", title);
			node.put("type", "experiment_result");
			String csv = ParticipantResult.generateDelimited("\t", EXPERIMENT_ID,participant);
			//System.out.println(csv);
			node.put("body", csv);
			node.put("og_public", false);
			HashMap og_groups;
			og_groups = new HashMap();
			og_groups.put(EXPERIMENT_GID, EXPERIMENT_GID);
			node.put("og_groups", og_groups);
			og_groups = new HashMap();
			og_groups.put(EXPERIMENT_GID, EXPERIMENT_NAME);
			node.put("og_groups_both", og_groups);
			d.nodeSave(node);
		} catch (MalformedURLException e) {
			printError(e);
		} catch (XmlRpcException e) {
			printError(e);
			System.err.println(e.getMessage());
		}		
	}

	public void hideBrowser() {
		//shell.setFullScreen(false);
		shell.setVisible(false);
	}

	/*public Map<String, Map<String, String>> getResults() {
		Map<String, Map<String, String>> results = new TreeMap<String, Map<String, String>>();
		if (!nasa_tlx.biasTally.isEmpty())
			results.put("nasatlx-biastally", nasa_tlx.getBiasTally());
		if (!nasa_tlx.taskRatings.isEmpty()) {
				
		}
		if (!times.isEmpty()) {
			results.put("times",times);
		}
		return results;
	}*/

	public void showOBG() {
		browser.setUrl("http://offbeatguides.com");
	}

	public void calculateTime() {
		int total = (int)System.currentTimeMillis()-time;
		times.put("Time " + (times.size() + 1), String.valueOf(total));
	}

	public void setStartTime() {
		time = (int)System.currentTimeMillis();
	}

	public void newParticipant() {
		ParticipantGUI pg = new ParticipantGUI(WindowConstants.EXIT_ON_CLOSE);
		pg.login(this);
		while (participant==null)
			Thread.yield();
		pg.survey.main.dispose();
		pg.survey.main = null;
		pg.survey = null;
	}
	
	public void newBackgroundInformation() {
		ParticipantGUI pg = new ParticipantGUI(WindowConstants.DO_NOTHING_ON_CLOSE);
		pg.newSurvey(BACKGROUNDINFO, background_survey.get(BACKGROUNDINFO), this);
		while (participant.results.get("Background Information")==null)
			Thread.yield();
		pg.survey.main.dispose();
		pg.survey.main = null;
		pg.survey = null;
	}

	public void newUsabilitySurvey() {
		ParticipantGUI pg = new ParticipantGUI(WindowConstants.DO_NOTHING_ON_CLOSE);
		for (String section : usability_survey.keySet()) {
			String title = "Purdue Usability Testing Questionnaire - " + section;
			pg.newSurvey(title, usability_survey.get(section), this);
			while (participant.results.get(title)==null)
				Thread.yield();
			pg.survey.main.dispose();
			pg.survey.main = null;
			pg.survey = null;
		}
	}
	
	public void newPairwiseComparison() {
		NASA_TLX nasatlx = new NASA_TLX();
		PairWiseComparison pwc = nasatlx.new PairWiseComparison(true, NASA_TLX.default_subscales, this);
		while (participant.results.get("NASA-TLX Bias Tally")==null)
			Thread.yield();
		pwc.main.dispose();
		pwc.main = null;
		nasatlx = null;
	}
	
	public void newTaskRating() {
		taskcount++;
		NASA_TLX nasatlx = new NASA_TLX();
		TaskRating tr = nasatlx.new TaskRating(true, this);
		while (participant.results.get("NASA-TLX Task["+taskcount+"] Subscale Ratings")==null)
			Thread.yield();
		tr.main.dispose();
		tr.main = null;
		nasatlx = null;
	}
		
	public static void main(String [] args) {
		System.setProperty("swt.library.path",".");
		new OffbeatGuides();
	}

	private void login(Map<String, String> result) {
		DrupalClient drupal = null;
		try {
			drupal = new DrupalClient(XMLRPC_URL);
			result = drupal.login(result.get("Username"), result.get("Password"));
		} catch (MalformedURLException e1) {
			printError(e1);
			return;
		} catch (XmlRpcException e1) {
			printError(e1);
			return;
		}
		if (result==null || drupal==null)
			return;
		if (drupal.getUser().get("og_groups") instanceof Map) {
			Map groups = (Map)drupal.getUser().get("og_groups");
			boolean isRegistered = false;
			for (Object g : groups.keySet()) {
				Map m = (Map)groups.get(g);
				String group = (String)m.get("type");
				group = group.trim();
				if (group.compareTo(EXPERIMENT_ID)==0) {
					isRegistered = true;
				}
			}
			if (isRegistered) {
				participant = new Participant(Integer.valueOf((String)drupal.getUser().get("uid")));
			} else {
				JOptionPane.showMessageDialog(null, "Please go to http://research.ryanhope.us and join:\n " +
						"\"OffbeatGuides.com Usability Test\"", null, JOptionPane.WARNING_MESSAGE);
			}
		}
	}
	
	public void update(Observable o, Object arg) {
		if (o instanceof PairWiseComparison) {
			participant.results.put("NASA-TLX Bias Tally", (Map<String, String>)arg);
		} else if (o instanceof TaskRating) {
			participant.results.put("NASA-TLX Task["+taskcount+"] Subscale Ratings", (Map<String, String>)arg);
		} else if (o instanceof Survey) {
			Survey survey = (Survey)o;
			if (survey.title.compareTo("Login Information")==0) {
				login((Map<String, String>)arg);
			} else 
				participant.results.put(survey.title, (Map<String, String>)arg);
					
		}
	}

}
