package us.ryanhope.experiment;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

public class Survey extends Observable {
	
	public enum QuestionType {
		FIXED_CHOICE,
		LIKERT1,
		LIKERT2,
		TEXT,
		PASSWORD
	}

	public Vector<AttributePanel> panels = null;
	public JButton submit = null;
	public String title = null;
	public JFrame main = null;
	
	public class AttributePanel {
		public String label;
		public JLabel labelHTML;
		public JComponent[] components;
		public String[] choices;
		int height = 0;
		public ButtonGroup group = null;
		public AttributePanel(SurveyQuestion question) {
			label = question.question;
			labelHTML = new JLabel("<html>" + label + "</html>");
			double h = label.length()/30;
			if (h>(int)h) h = (int)h+1;
			if (h>height)
				height = (int)h;
			if (h>1)
				labelHTML.setPreferredSize(new Dimension(150,50));
			if (question.type == QuestionType.TEXT) {
				JTextField tf = new JTextField(15);
				components = new JComponent[]{tf};
			} else if (question.type == QuestionType.PASSWORD) {
				JPasswordField pf = new JPasswordField(15);
				components = new JComponent[]{pf};
			} else if (question.type == QuestionType.FIXED_CHOICE) {
				choices = question.choices;
				if (choices.length > 0)
					components = new JComponent[]{new JComboBox(choices)};			
			} else if (question.type == QuestionType.LIKERT1) {
				group = new ButtonGroup();
				components = new JComponent[8];
				for (int i=0; i<8; i++) {
					JRadioButton button = new JRadioButton();
					button.setActionCommand(String.valueOf(i+1));
					group.add(button);
					components[i] = button;
				}
			}
		}
	}
	
	public TreeMap<String, String> getResults() {
		TreeMap<String, String> results = new TreeMap<String, String>();
		for (AttributePanel p : panels){
			String key = p.label;
			String value = null;
			if (p.components[0] instanceof JComboBox) {
				JComboBox cb = (JComboBox)p.components[0];
				int index = cb.getSelectedIndex();
				value = p.choices[index];
			} else if (p.components[0] instanceof JPasswordField) {
				JPasswordField pf =(JPasswordField)p.components[0];
				value = String.valueOf(pf.getPassword());
			} else if (p.components[0] instanceof JTextField) {
				JTextField tf =(JTextField)p.components[0];
				value = tf.getText();
			} else if (p.components[0] instanceof JRadioButton) {
				value = p.group.getSelection().getActionCommand();
			}
			results.put(key, value);
		}
		return results;				
	}
	
	public boolean validatePanel(AttributePanel panel) {
		boolean ret = true;
		if (panel.components[0] instanceof JRadioButton) {
			if (panel.group.getSelection()==null)
				ret = false;
		} else if (panel.components[0] instanceof JTextField) {
			JTextField tf = (JTextField)panel.components[0];
			if (tf.getText().trim().compareTo("")==0)
				ret = false;
		} else if (panel.components[0] instanceof JComboBox) {
			JComboBox cb = (JComboBox)panel.components[0];
			String text = (String)cb.getSelectedItem();
			if (text.trim().compareTo("")==0)
				ret = false;
		} else if (panel.components[0] instanceof JPasswordField) {
			JPasswordField pf = (JPasswordField)panel.components[0];
			String text = String.valueOf(pf.getPassword());
			if (text.trim().compareTo("")==0)
				ret = false;
		}
		return ret;
	}
	
	public Survey(String title, Vector<SurveyQuestion> questions, int closeAction, Observer o) {
		if (o!=null)
			addObserver(o);
		this.title = title;
		main = new JFrame();
		main.setDefaultCloseOperation(closeAction);
		main.setLayout(new FlowLayout());
		panels = new Vector<AttributePanel>();
		JPanel panel = new JPanel();
		for (SurveyQuestion question : questions) {
			AttributePanel ap = new AttributePanel(question);
			panels.add(ap);
			panel.add(ap.labelHTML);
			if (ap.components.length>1) {
				JPanel l = new JPanel(new FlowLayout());
				if (question.type==Survey.QuestionType.LIKERT1) {
					l.add(new JLabel("bad"));
					for (JComponent c : ap.components)
						l.add(c);
					l.add(new JLabel("good"));
					panel.add(l);
				} else {
					panel.add(l);
				}
			} else {
				panel.add(ap.components[0]);
			}
		}
		panel.setLayout(new GridLayout(questions.size(),2,15,5));
		JScrollPane pane = new JScrollPane(panel);
		pane.setBorder(BorderFactory.createTitledBorder(new CompoundBorder(
				BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
						title, TitledBorder.CENTER, TitledBorder.CENTER), 
						new EmptyBorder(10,20,10,20))));
		submit = new JButton("Submit");
		submit.setActionCommand(title);
		ActionListener l = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean pass = true;
				for (AttributePanel panel : panels) {
					if (!validatePanel(panel))
						pass = false;
				}
				if (pass) {
					setChanged();
					notifyObservers(getResults());
				}
			}
		};
		submit.addActionListener(l);
		Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
		main.add(pane);
		main.add(submit);
		main.pack();
		/*if (getSize().height>(2*screensize.height/3)) {
			pane.setPreferredSize(new Dimension(getSize().width,2*screensize.height/3));
			pack();
		}
		if (getSize().width>(2*screensize.width/3)) {
			pane.setPreferredSize(new Dimension(2*screensize.height/3,getSize().height));
			pack();
		}*/
		main.setLocation(screensize.width/2-main.getWidth()/2,screensize.height/2-main.getHeight()/2);
		main.setResizable(false);
		main.setVisible(true);
	}

}
