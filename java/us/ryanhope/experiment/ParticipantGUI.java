/**
 * ParticipantGUI.java
 */

/*
Copyright (c) 2008, Ryan M. Hope
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
	  this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
 * Neither the name of the project nor the names of its contributors may be
	  used to endorse or promote products derived from this software without
	  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package us.ryanhope.experiment;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observer;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;

public class ParticipantGUI {
	
	private int closeAction;
	public Survey survey;
	
	public ParticipantGUI(int closeAction) {
		this.closeAction = closeAction;
	}
	
	static HashMap<String, String[]> parseFile(File file) {
		HashMap<String, String[]> questions = new HashMap<String, String[]>();
		try {
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				//ArrayList<String> list = new ArrayList<String>();
				StringTokenizer attr = 
					new StringTokenizer(scanner.nextLine(),"|");
				String label = (String)attr.nextElement();
				ArrayList<String> question = new ArrayList<String>();
				while(attr.hasMoreTokens()){
					String choices = (String)attr.nextElement();
					StringTokenizer choice = 
						new StringTokenizer(choices,",");
					while(choice.hasMoreTokens()) {
						question.add((String)choice.nextElement());
					}
				}
				questions.put(label, (String[])question.toArray());
			}
		} catch (FileNotFoundException e) {
			System.err.println("File \"" + file.getName() + "\" not found!");
			System.exit(2);
		}
		return questions;
	}
	
	public void newSurvey(String title, Vector<SurveyQuestion> questions, 
			Observer o) {
		survey = new Survey(title, questions, closeAction, o);		
	}
	
	public void login(Observer o) {
		Vector<SurveyQuestion> questions = new Vector<SurveyQuestion>();
		questions.add(new SurveyQuestion(Survey.QuestionType.TEXT, "Username", null));
		questions.add(new SurveyQuestion(Survey.QuestionType.PASSWORD, "Password", null));
		newSurvey("Login Information", questions, o);
	}
	
}
