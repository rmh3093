/**
 * ParticipantResult.java
 */

/*
Copyright (c) 2008, Ryan M. Hope
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
	  this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
 * Neither the name of the project nor the names of its contributors may be
	  used to endorse or promote products derived from this software without
	  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package us.ryanhope.experiment;

import java.io.StringWriter;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.SAXException;


import com.sun.xml.internal.bind.util.AttributesImpl;

public class ParticipantResult {

	public static String generateXML(String experiment, Participant p) {
		StringWriter output = new StringWriter();
		try {
			StreamResult streamResult = new StreamResult(output);
			SAXTransformerFactory tf = (SAXTransformerFactory) SAXTransformerFactory.newInstance();
			tf.setAttribute("indent-number", new Integer(2));
			TransformerHandler hd = null;
			hd = tf.newTransformerHandler();
			Transformer serializer = hd.getTransformer();
			serializer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
			serializer.setOutputProperty(OutputKeys.INDENT,"yes");
			hd.setResult(streamResult);
			hd.startDocument();
			AttributesImpl atts = new AttributesImpl();

			hd.startElement("","",experiment,atts);

			atts.clear();

			atts.addAttribute("","","id","CDATA",String.valueOf(p.pid));
			hd.startElement("","","participant",atts);

			atts.clear();
			hd.startElement("","","results",atts);
			atts.clear();
			for (String r_key : p.results.keySet()) {
				Map<String, String> r = p.results.get(r_key);
				for (String s : r.keySet()) {
					atts.addAttribute("","",s,"CDATA",r.get(s));
				}
				hd.startElement("","",r_key,atts);
				hd.endElement("","",r_key);

			}
			hd.endElement("","","results");

			hd.endElement("","","participant");

			hd.endElement("","",experiment);
			hd.endDocument();

		} catch (SAXException e) {
		} catch (TransformerConfigurationException e) {
		}

		return output.toString();

	}

	public static String generateDelimited(String delim, String experiment, Participant p) {
		StringBuffer output = new StringBuffer("experiment_id" + delim + experiment + "\n");
		output.append("participant_id" + delim + String.valueOf(p.pid) + "\n");
		output.append("\n");
		output.append("results\n\n");
		for (String r_key : p.results.keySet()) {
			output.append(delim + r_key + "\n");
			Map<String, String> r = p.results.get(r_key);
			for (String s : r.keySet()) {
				output.append(delim + delim + s + delim + r.get(s) + "\n");
			}
			output.append("\n");
		}
		return output.toString();
	}


}
