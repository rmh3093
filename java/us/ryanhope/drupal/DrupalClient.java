package us.ryanhope.drupal;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

@SuppressWarnings("unchecked")
public class DrupalClient {

	private String sessid = null;
	private XmlRpcClient client = null;
	private Map user = null;

	public String getSessid() { return sessid; };
	public XmlRpcClient getClient() { return client; };
	public Map getUser() { return user; };

	public DrupalClient(String url) throws MalformedURLException, XmlRpcException {
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		config.setServerURL(new URL(url));
		client = new XmlRpcClient();
		client.setConfig(config);
		Map result = (HashMap)client.execute("system.connect", new Object[]{});
		sessid = (String)result.get("sessid");		
	}

	public Map login(String username, String password) throws XmlRpcException {
		Map login = null;
		login = (HashMap)client.execute("user.login", new Object[]{sessid,username,password});
		sessid = (String)login.get("sessid");
		user = (HashMap)login.get("user");
		return login;
	}
	
	public Object nodeGet(int nid, Object[]fields) throws XmlRpcException {
		return client.execute("node.get", new Object[]{sessid,nid, fields});
	}
	
	public Object nodeSave(Map node) throws XmlRpcException {
		return (String)client.execute("node.save", new Object[]{sessid,node});
	}

	public static void main(String[] args) {
		try {
			DrupalClient dc = new DrupalClient("http://ryanhope.us/services/xmlrpc");
			dc.login(args[0], args[1]);
			System.out.println((Map)dc.nodeGet(79, new Object[]{}));
		} catch (MalformedURLException e) {
			System.err.println(e);
		} catch (XmlRpcException e) {
			System.err.println(e);
		}
	}
	
}
